<?php
/**
 * Created by IntelliJ IDEA.
 * User: depekur
 * Date: 26.07.2018
 * Time: 13:45
 */

class ControllerExtensionModuleMainProducts extends Controller
{
  private $error = array();

  public function index()
  {
    $this->load->language('extension/module/main_products');

    $this->document->setTitle($this->language->get('heading_title'));

    $data['heading_title'] = $this->language->get('heading_title');
// Текст кнопки Редактировать
    $data['text_edit'] = $this->language->get('text_edit');
// Текст "Включено"
    $data['text_enabled'] = $this->language->get('text_enabled');
// Текст "Выключено"
    $data['text_disabled'] = $this->language->get('text_disabled');
// Текст "Установить"
    $data['text_signup'] = $this->language->get('text_signup');
// Текст метки поля "Код"
    $data['entry_code'] = $this->language->get('entry_code');
// Текст метки переключателя статуса модуля
    $data['entry_status'] = $this->language->get('entry_status');
// Текст кнопки "Сохранить"
    $data['button_save'] = $this->language->get('button_save');
// Текст кнопки "Отменить"
    $data['button_cancel'] = $this->language->get('button_cancel');


    $this->load->model('setting/setting');
    $this->load->model('setting/module');


    if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
      if (!isset($this->request->get['module_id'])) {
        $this->model_setting_module->addModule('main_products', $this->request->post);
      } else {
        $this->model_setting_module->editModule($this->request->get['module_id'], $this->request->post);
      }

      $this->session->data['success'] = $this->language->get('text_success');

      $this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
    }






    if (isset($this->error['warning'])) {
      $data['error_warning'] = $this->error['warning'];
    } else {
      $data['error_warning'] = '';
    }
    if (isset($this->error['code'])) {
      $data['error_code'] = $this->error['code'];
    } else {
      $data['error_code'] = '';
    }





    if (!isset($this->request->get['module_id'])) {
      $data['action'] = $this->url->link('extension/module/main_products', 'user_token=' . $this->session->data['user_token'], true);
    } else {
      $data['action'] = $this->url->link('extension/module/main_products', 'user_token=' . $this->session->data['user_token'] . '&module_id=' . $this->request->get['module_id'], true);
    }

    $data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

    if (isset($this->request->get['module_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
      $module_info = $this->model_setting_module->getModule($this->request->get['module_id']);
    }

    $data['user_token'] = $this->session->data['user_token'];

    if (isset($this->request->post['name'])) {
      $data['name'] = $this->request->post['name'];
    } elseif (!empty($module_info)) {
      $data['name'] = $module_info['name'];
    } else {
      $data['name'] = '';
    }


//    $data['breadcrumbs'] = array();
//    $data['breadcrumbs'][] = array(
//      'text' => $this->language->get('text_home'),
//      'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
//    );
//    $data['breadcrumbs'][] = array(
//      'text' => $this->language->get('text_extension'),
//      'href' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=modules', true)
//    );
//    $data['breadcrumbs'][] = array(
//      'text' => $this->language->get('heading_title'),
//      'href' => $this->url->link('extension/modules/mainproducts', 'token=' . $this->session->data['token'] . '&store_id=' . $this->request->get['store_id'], true)
//    );

    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');

    $this->response->setOutput($this->load->view('extension/module/main_products', $data));

  }

  protected function validate()
  {
    if (!$this->user->hasPermission('modify', 'module/main_products')) {
      //$this->error['warning'] = $this->language->get('error_permission');
    }
    return !$this->error;
  }
}