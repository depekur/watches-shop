$( document ).ready(function() {
    toggleSearchForm();

    function toggleSearchForm() {
        var searchButton = $('.search-button'),
            searchForm = $('#search'),
            searchBg = $('.search-bg');

        searchButton.on( "click", function() {
            searchForm.fadeIn();
            searchBg.fadeIn();
        });
        searchBg.on( "click", function() {
            searchForm.fadeOut();
            searchBg.fadeOut();
        });

    }

});