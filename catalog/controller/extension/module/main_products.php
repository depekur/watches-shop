<?php
/**
 * Created by IntelliJ IDEA.
 * User: depekur
 * Date: 26.07.2018
 * Time: 13:45
 */

class ControllerExtensionModuleMainProducts extends Controller {

  public function index()
  {
    $this->load->language('extension/module/main_products');

    $this->load->model('catalog/category');
    $this->load->model('catalog/product');
    $this->load->model('tool/image');

    $data = array();
    $data['main_products'] = array();

    $categories = $this->model_catalog_category->getCategories(0);




    foreach ($categories as $category) {
      $params = array(
        'filter_category_id' => $category['category_id'],
        'limit' => 4,
        'start' => 0
      );

      $products = $this->getProducts($params);

      if (count($products)) {
        $data['main_products'][] = array(
          'category' => $category['name'],
          'products' => $products
        );
      }
    }

    //$data['categories'] = $categories;

    return $this->load->view('extension/module/main_products', $data);
  }

  private function getProducts($params) {
    $setting = array(
      'width' => 200,
      'height' => 200
    );

    $data['products'] = array();

    $products = $this->model_catalog_product->getProducts($params);

//    echo '<pre>';
//    print_r($products);
//    echo '</pre>';

    foreach ($products as $product_info) {
      //$product_info = $this->model_catalog_product->getProduct($product['product_id']);

      //print_r($product_info);

      if ($product_info) {
        if ($product_info['image']) {
          $image = $this->model_tool_image->resize($product_info['image'], $setting['width'], $setting['height']);
        } else {
          $image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
        }

        if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
          $price = $this->currency
                        ->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
        } else {
          $price = false;
        }

        if ((float)$product_info['special']) {
          $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
        } else {
          $special = false;
        }

        if ($this->config->get('config_tax')) {
          $tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
        } else {
          $tax = false;
        }

        if ($this->config->get('config_review_status')) {
          $rating = $product_info['rating'];
        } else {
          $rating = false;
        }

        $data['products'][] = array(
          'product_id'  => $product_info['product_id'],
          'thumb'       => $image,
          //'thumb'       => $product_info['image'],
          'name'        => $product_info['name'],
          'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
          'price'       => $price,
          'special'     => $special,
          'tax'         => $tax,
          'rating'      => $rating,
          'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
        );
      }
    }

    return $data['products'];
  }
}